import lt.kudze.stream.device.connector.DeviceConnectorStream;
import lt.kudze.stream.device.connector.messages.device.DeviceSensorValue;
import lt.kudze.stream.device.connector.messages.internal.DeviceConnectorAction;
import lt.kudze.stream.device.connector.messages.output.OutputDeviceValue;
import lt.kudze.stream.device.connector.topics.DebeziumDeviceSensorTopic;
import lt.kudze.stream.device.connector.topics.DebeziumDeviceTopic;
import lt.kudze.stream.device.connector.topics.OutputDevicesTopic;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import resources.input.InputDeviceSensors;
import resources.input.InputDevices;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.Properties;

public class DeviceConnectorStreamTest {

    @Test
    public void testEndToEnd() throws IOException {
        System.out.println("Setup topology driver");
        Properties config = new Properties();
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, "test");
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9999"); // not existing server
        config.put(StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG,
                org.apache.kafka.streams.errors.LogAndContinueExceptionHandler.class.getName()
        );

        var topology = DeviceConnectorStream.buildTopology().build();
        System.out.println(topology.describe());

        TopologyTestDriver testDriver = new TopologyTestDriver(topology, config);
//        testDriver.close();

        var inputDeviceTopic = testDriver.createInputTopic(DebeziumDeviceTopic.NAME, Serdes.String().serializer(), Serdes.String().serializer());
        var inputSensorTopic = testDriver.createInputTopic(DebeziumDeviceSensorTopic.NAME, Serdes.String().serializer(), Serdes.String().serializer());
        var outputTopic = testDriver.createOutputTopic(OutputDevicesTopic.NAME, Serdes.String().deserializer(), OutputDeviceValue.serde.deserializer());

        //Testing create with random device that does not have any sensors.
        inputDeviceTopic.pipeInput(
                getResource(InputDevices.NO_SENSORS_KEY),
                getResource(InputDevices.NO_SENSORS_VALUE)
        );

        var record = outputTopic.readRecord();
        Assertions.assertEquals("9ae3d147-db51-4bab-9783-5370547f90cd", record.key());
        Assertions.assertEquals("9ae3d147-db51-4bab-9783-5370547f90cd", record.value().getUuid());
        Assertions.assertEquals("{\"lamps\": [false, false]}", record.value().getConfig());
        Assertions.assertSame(0, record.value().getSensors().length);
        Assertions.assertSame(DeviceConnectorAction.SYNC, record.value().getNeededAction());
        Assertions.assertEquals("$argon2id$v=19$m=65536,t=4,p=1$UkFyaE93VExOcEJHeUdoZw$42gGUJ4uCSANOYen1vJe7MkhBnOtzNgJbacwYvA+TPE", record.value().getApiKeyHash());
        Assertions.assertEquals("2023-12-20T00:36:34Z", record.value().getCreatedAt());
        Assertions.assertEquals("2024-05-02T06:20:47Z", record.value().getUpdatedAt());
        Assertions.assertTrue(outputTopic.isEmpty());

        //Testing delete with random device that does not have any sensors.
        inputDeviceTopic.pipeInput(
                getResource(InputDevices.NO_SENSORS_KEY),
                getResource(InputDevices.NO_SENSORS_DELETE_VALUE)
        );
        record = outputTopic.readRecord();
        Assertions.assertEquals("9ae3d147-db51-4bab-9783-5370547f90cd", record.key());
        Assertions.assertNull(record.value());
        Assertions.assertTrue(outputTopic.isEmpty());

        //Testing tombstone message.
        inputDeviceTopic.pipeInput(
                getResource(InputDevices.NO_SENSORS_KEY),
                "null"
        );
        Assertions.assertTrue(outputTopic.isEmpty());

        //Testing device with sensors
        inputDeviceTopic.pipeInput(
                getResource(InputDevices.WITH_SENSORS_KEY),
                getResource(InputDevices.WITH_SENSORS_VALUE)
        );

        record = outputTopic.readRecord();
        Assertions.assertEquals("9ae3d147-db51-4bab-9783-5370547f90cf", record.key());
        Assertions.assertEquals("9ae3d147-db51-4bab-9783-5370547f90cf", record.value().getUuid());
        Assertions.assertEquals("{\"lamps\": [false, false]}", record.value().getConfig());
        Assertions.assertSame(0, record.value().getSensors().length);
        Assertions.assertSame(DeviceConnectorAction.SYNC, record.value().getNeededAction());
        Assertions.assertEquals("$argon2id$v=19$m=65536,t=4,p=1$UkFyaE93VExOcEJHeUdoZw$42gGUJ4uCSANOYen1vJe7MkhBnOtzNgJbacwYvA+TPE", record.value().getApiKeyHash());
        Assertions.assertEquals("2023-12-20T00:36:34Z", record.value().getCreatedAt());
        Assertions.assertEquals("2023-12-20T00:36:34Z", record.value().getUpdatedAt());
        Assertions.assertTrue(outputTopic.isEmpty());

        //Lets add first sensor
        inputSensorTopic.pipeInput(
                getResource(InputDeviceSensors.WITH_SENSORS_1_KEY),
                getResource(InputDeviceSensors.WITH_SENSORS_1_VALUE)
        );
        record = outputTopic.readRecord();
        Assertions.assertEquals("9ae3d147-db51-4bab-9783-5370547f90cf", record.key());
        Assertions.assertEquals("9ae3d147-db51-4bab-9783-5370547f90cf", record.value().getUuid());
        Assertions.assertEquals("{\"lamps\": [false, false]}", record.value().getConfig());
        Assertions.assertSame(1, record.value().getSensors().length);
        Assertions.assertSame(DeviceConnectorAction.DISCONNECT, record.value().getNeededAction());
        Assertions.assertEquals("$argon2id$v=19$m=65536,t=4,p=1$UkFyaE93VExOcEJHeUdoZw$42gGUJ4uCSANOYen1vJe7MkhBnOtzNgJbacwYvA+TPE", record.value().getApiKeyHash());
        Assertions.assertEquals("2023-12-20T00:36:34Z", record.value().getCreatedAt());
        Assertions.assertEquals("2023-12-20T00:36:34Z", record.value().getUpdatedAt());
        Assertions.assertTrue(outputTopic.isEmpty());

        //Lets add second sensor
        inputSensorTopic.pipeInput(
                getResource(InputDeviceSensors.WITH_SENSORS_2_KEY),
                getResource(InputDeviceSensors.WITH_SENSORS_2_VALUE)
        );
        record = outputTopic.readRecord();
        Assertions.assertEquals("9ae3d147-db51-4bab-9783-5370547f90cf", record.key());
        Assertions.assertEquals("9ae3d147-db51-4bab-9783-5370547f90cf", record.value().getUuid());
        Assertions.assertEquals("{\"lamps\": [false, false]}", record.value().getConfig());
        Assertions.assertSame(2, record.value().getSensors().length);
        Assertions.assertSame(DeviceConnectorAction.DISCONNECT, record.value().getNeededAction());
        Assertions.assertEquals("$argon2id$v=19$m=65536,t=4,p=1$UkFyaE93VExOcEJHeUdoZw$42gGUJ4uCSANOYen1vJe7MkhBnOtzNgJbacwYvA+TPE", record.value().getApiKeyHash());
        Assertions.assertEquals("2023-12-20T00:36:34Z", record.value().getCreatedAt());
        Assertions.assertEquals("2023-12-20T00:36:34Z", record.value().getUpdatedAt());
        Assertions.assertTrue(outputTopic.isEmpty());

        //Lets remove first sensor
        inputSensorTopic.pipeInput(
                getResource(InputDeviceSensors.WITH_SENSORS_1_KEY),
                getResource(InputDeviceSensors.WITH_SENSORS_1_DELETE_VALUE)
        );
        record = outputTopic.readRecord();
        Assertions.assertEquals("9ae3d147-db51-4bab-9783-5370547f90cf", record.key());
        Assertions.assertEquals("9ae3d147-db51-4bab-9783-5370547f90cf", record.value().getUuid());
        Assertions.assertEquals("{\"lamps\": [false, false]}", record.value().getConfig());
        Assertions.assertSame(1, record.value().getSensors().length);
        Assertions.assertSame(DeviceConnectorAction.DISCONNECT, record.value().getNeededAction());
        Assertions.assertEquals("$argon2id$v=19$m=65536,t=4,p=1$UkFyaE93VExOcEJHeUdoZw$42gGUJ4uCSANOYen1vJe7MkhBnOtzNgJbacwYvA+TPE", record.value().getApiKeyHash());
        Assertions.assertEquals("2023-12-20T00:36:34Z", record.value().getCreatedAt());
        Assertions.assertEquals("2023-12-20T00:36:34Z", record.value().getUpdatedAt());
        Assertions.assertTrue(outputTopic.isEmpty());

        //Just some random update that should be skipped
        inputDeviceTopic.pipeInput(
                getResource(InputDevices.WITH_SENSORS_KEY),
                getResource(InputDevices.WITH_SENSORS_UPDATE_SKIP_VALUE)
        );
        Assertions.assertTrue(outputTopic.isEmpty());

        testDriver.close();
    }

    private String getResource(String resource) throws IOException {
        return Files.readString(Path.of(Objects.requireNonNull(this.getClass().getResource("/messages/" + resource)).getPath()));
    }
}

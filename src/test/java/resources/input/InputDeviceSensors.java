package resources.input;

public class InputDeviceSensors {
    public static final String WITH_SENSORS_1_KEY = "input/sensors/withSensors/1.key.json";
    public static final String WITH_SENSORS_1_VALUE = "input/sensors/withSensors/1.json";
    public static final String WITH_SENSORS_1_DELETE_VALUE = "input/sensors/withSensors/1Delete.json";

    public static final String WITH_SENSORS_2_KEY = "input/sensors/withSensors/2.key.json";
    public static final String WITH_SENSORS_2_VALUE = "input/sensors/withSensors/2.json";
}

package resources.input;

public class InputDevices {
    public static final String NO_SENSORS_KEY = "input/devices/noSensors.key.json";
    public static final String NO_SENSORS_VALUE = "input/devices/noSensors.json";
    public static final String NO_SENSORS_DELETE_VALUE = "input/devices/noSensorsDelete.json";

    public static final String WITH_SENSORS_KEY = "input/devices/withSensors.key.json";
    public static final String WITH_SENSORS_VALUE = "input/devices/withSensors.json";
    public static final String WITH_SENSORS_UPDATE_SKIP_VALUE = "input/devices/withSensorsUpdateSkip.json";
}

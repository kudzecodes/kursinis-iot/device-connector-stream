package lt.kudze.stream.device.connector.topics;

import lt.kudze.stream.device.connector.messages.debezium.DebeziumDeviceSensorKey;
import lt.kudze.stream.device.connector.messages.debezium.DebeziumDeviceSensorValue;
import lt.kudze.stream.device.connector.util.SystemUtil;
import org.apache.kafka.streams.kstream.Consumed;

public class DebeziumDeviceSensorTopic {
    public static final String NAME = SystemUtil.getenv("KAFKA_DEBEZIUM_DEVICE_SENSOR_TOPIC");
    public static final Consumed<DebeziumDeviceSensorKey, DebeziumDeviceSensorValue> CONSUMED = Consumed.with(DebeziumDeviceSensorKey.serde, DebeziumDeviceSensorValue.serde);
}

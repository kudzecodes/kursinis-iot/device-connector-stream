package lt.kudze.stream.device.connector.topics;

import lt.kudze.stream.device.connector.messages.device.DeviceKey;
import lt.kudze.stream.device.connector.messages.output.OutputDeviceValue;
import lt.kudze.stream.device.connector.util.SystemUtil;
import org.apache.kafka.streams.kstream.Produced;

public class OutputDevicesTopic {
    public static final String NAME = SystemUtil.getenv("KAFKA_OUTPUT_DEVICES_TOPIC");
    public static final Produced<DeviceKey, OutputDeviceValue> PRODUCED = Produced.with(DeviceKey.serde, OutputDeviceValue.serde);
}

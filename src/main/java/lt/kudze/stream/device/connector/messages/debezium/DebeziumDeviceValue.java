package lt.kudze.stream.device.connector.messages.debezium;

import lt.kudze.stream.device.connector.messages.device.DeviceValue;
import lt.kudze.stream.device.connector.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DebeziumDeviceValue extends DebeziumValue<DeviceValue> {
    public static final Serde<DebeziumDeviceValue> serde = JsonSerdes.of(DebeziumDeviceValue.class);
}

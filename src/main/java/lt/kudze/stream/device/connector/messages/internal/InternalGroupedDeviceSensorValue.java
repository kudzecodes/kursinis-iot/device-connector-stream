package lt.kudze.stream.device.connector.messages.internal;

import lt.kudze.stream.device.connector.messages.device.DeviceSensorValue;
import lt.kudze.stream.device.connector.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class InternalGroupedDeviceSensorValue extends AggregatedValues<DeviceSensorValue> {
    public static final Serde<InternalGroupedDeviceSensorValue> serde = JsonSerdes.of(InternalGroupedDeviceSensorValue.class);

    private String lastModified = null;
    private DeviceConnectorAction neededAction = DeviceConnectorAction.SYNC;

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public DeviceConnectorAction getNeededAction() {
        return neededAction;
    }

    public void setNeededAction(DeviceConnectorAction neededAction) {
        this.neededAction = neededAction;
    }
}

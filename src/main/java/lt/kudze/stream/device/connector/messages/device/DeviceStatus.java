package lt.kudze.stream.device.connector.messages.device;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum DeviceStatus {
    @JsonProperty("allocating")
    ALLOCATING,
    @JsonProperty("allocated")
    ALLOCATED,
    @JsonProperty("online")
    ONLINE,
    @JsonProperty("offline")
    OFFLINE,
}

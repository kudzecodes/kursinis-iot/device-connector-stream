package lt.kudze.stream.device.connector.messages.internal;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Defines the action that the device connector should take
 */
public enum DeviceConnectorAction {
    //Should only sync data.
    @JsonProperty("sync")
    SYNC,
    //Should sync data and reconfigure the device.
    @JsonProperty("reconfigure")
    RECONFIGURE,
    //Should sync data and disconnect the device.
    @JsonProperty("disconnect")
    DISCONNECT

}

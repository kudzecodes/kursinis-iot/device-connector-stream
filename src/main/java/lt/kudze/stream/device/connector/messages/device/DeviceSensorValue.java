package lt.kudze.stream.device.connector.messages.device;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lt.kudze.stream.device.connector.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DeviceSensorValue {
    public static final Serde<DeviceSensorValue> serde = JsonSerdes.of(DeviceSensorValue.class);

    private String uuid;
    private String createdAt;
    private String updatedAt;
    private String deviceUuid;
    private String jsonpathQuery;

    public DeviceSensorValue() {

    }

    public DeviceSensorValue(DeviceSensorValue value) {
        this.uuid = value.uuid;
        this.createdAt = value.createdAt;
        this.updatedAt = value.updatedAt;
        this.deviceUuid = value.deviceUuid;
        this.jsonpathQuery = value.jsonpathQuery;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeviceUuid() {
        return deviceUuid;
    }

    @JsonIgnore
    public DeviceKey constructDeviceKey() {
        return new DeviceKey(this.deviceUuid);
    }

    public void setDeviceUuid(String deviceUuid) {
        this.deviceUuid = deviceUuid;
    }

    public String getJsonpathQuery() {
        return jsonpathQuery;
    }

    public void setJsonpathQuery(String jsonpathQuery) {
        this.jsonpathQuery = jsonpathQuery;
    }
}

package lt.kudze.stream.device.connector.messages.internal;

import lt.kudze.stream.device.connector.messages.device.DeviceValue;
import lt.kudze.stream.device.connector.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class InternalDeviceValue extends DeviceValue {
    public static final Serde<InternalDeviceValue> serde = JsonSerdes.of(InternalDeviceValue.class);

    public static DeviceConnectorAction computeNeededAction(DeviceValue device, DeviceValue beforeDevice) {
        //If before is empty it just needs to sync data.
        if (beforeDevice == null)
            return DeviceConnectorAction.SYNC;

        //If api key has changed then it needs to disconnect
        if (!beforeDevice.getApiKeyHash().equals(device.getApiKeyHash()))
            return DeviceConnectorAction.DISCONNECT;

        //If config has changed then it needs to reconfigure
        if (!beforeDevice.getConfig().equals(device.getConfig()))
            return DeviceConnectorAction.RECONFIGURE;

        return DeviceConnectorAction.SYNC;
    }

    private DeviceConnectorAction neededAction;

    public InternalDeviceValue() {

    }

    public InternalDeviceValue(DeviceValue value, DeviceConnectorAction neededAction) {
        super(value);

        this.neededAction = neededAction;
    }

    public InternalDeviceValue(InternalDeviceValue value) {
        super(value);

        this.neededAction = value.neededAction;
    }

    public DeviceConnectorAction getNeededAction() {
        return neededAction;
    }

    public void setNeededAction(DeviceConnectorAction neededAction) {
        this.neededAction = neededAction;
    }
}

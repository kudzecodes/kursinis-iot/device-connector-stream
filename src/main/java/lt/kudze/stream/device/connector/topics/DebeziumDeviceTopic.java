package lt.kudze.stream.device.connector.topics;

import lt.kudze.stream.device.connector.messages.debezium.DebeziumDeviceKey;
import lt.kudze.stream.device.connector.messages.debezium.DebeziumDeviceValue;
import lt.kudze.stream.device.connector.util.SystemUtil;
import org.apache.kafka.streams.kstream.Consumed;

public class DebeziumDeviceTopic {
    public static final String NAME = SystemUtil.getenv("KAFKA_DEBEZIUM_DEVICE_TOPIC");
    public static final Consumed<DebeziumDeviceKey, DebeziumDeviceValue> CONSUMED = Consumed.with(DebeziumDeviceKey.serde, DebeziumDeviceValue.serde);
}

package lt.kudze.stream.device.connector.messages.device;

import lt.kudze.stream.device.connector.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DeviceValue {
    public static final Serde<DeviceValue> serde = JsonSerdes.of(DeviceValue.class);

    private String uuid;
    private String config;
    private String createdAt;
    private String updatedAt;
    private String apiKeyHash;

    public DeviceValue() {

    }

    public DeviceValue(DeviceValue device) {
        this.uuid = device.uuid;
        this.config = device.config;
        this.createdAt = device.createdAt;
        this.updatedAt = device.updatedAt;
        this.apiKeyHash = device.apiKeyHash;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getApiKeyHash() {
        return apiKeyHash;
    }

    public void setApiKeyHash(String apiKeyHash) {
        this.apiKeyHash = apiKeyHash;
    }
}

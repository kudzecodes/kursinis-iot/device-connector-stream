package lt.kudze.stream.device.connector.messages.internal;

import java.util.HashMap;
import java.util.Map;

public abstract class AggregatedValues<Entity> {
    private Map<String, Entity> values = new HashMap<>();

    public Map<String, Entity> getValues() {
        return values;
    }

    public void setValues(Map<String, Entity> values) {
        this.values = values;
    }
}


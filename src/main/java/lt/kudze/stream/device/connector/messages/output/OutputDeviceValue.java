package lt.kudze.stream.device.connector.messages.output;

import lt.kudze.stream.device.connector.messages.device.DeviceSensorValue;
import lt.kudze.stream.device.connector.messages.internal.DeviceConnectorAction;
import lt.kudze.stream.device.connector.messages.internal.InternalDeviceValue;
import lt.kudze.stream.device.connector.messages.internal.InternalGroupedDeviceSensorValue;
import lt.kudze.stream.device.connector.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

import java.time.Instant;

public class OutputDeviceValue extends InternalDeviceValue {
    public static final Serde<OutputDeviceValue> serde = JsonSerdes.of(OutputDeviceValue.class);

    public static DeviceSensorValue[] computeSensorsFromGrouped(InternalGroupedDeviceSensorValue sensors) {
        if (sensors == null)
            return new DeviceSensorValue[0];

        return sensors.getValues().values().toArray(new DeviceSensorValue[0]);
    }

    public static DeviceConnectorAction computeNeededAction(InternalDeviceValue device, InternalGroupedDeviceSensorValue sensors) {
        if (sensors == null || sensors.getLastModified() == null)
            return device.getNeededAction();

        var deviceModifiedAt = Instant.parse(device.getUpdatedAt());
        var sensorsModifiedAt = Instant.parse(sensors.getLastModified());

        return sensorsModifiedAt.isAfter(deviceModifiedAt) ? sensors.getNeededAction() : device.getNeededAction();
    }

    private DeviceSensorValue[] sensors;

    public OutputDeviceValue() {

    }

    public OutputDeviceValue(InternalDeviceValue value, DeviceSensorValue[] sensors, DeviceConnectorAction neededAction) {
        super(value);

        this.sensors = sensors;
        this.setNeededAction(neededAction);
    }

    public DeviceSensorValue[] getSensors() {
        return sensors;
    }

    public void setSensors(DeviceSensorValue[] sensors) {
        this.sensors = sensors;
    }
}

package lt.kudze.stream.device.connector.messages.debezium;

public abstract class DebeziumKey<DataClass> {

    private final DebeziumKeySchema schema;
    private final DataClass payload;

    public DebeziumKey() {
        this(null, null);
    }

    public DebeziumKey(DebeziumKeySchema schema, DataClass payload) {
        this.schema = schema;
        this.payload = payload;
    }

    public DebeziumKeySchema getSchema() {
        return schema;
    }

    public DataClass getPayload() {
        return payload;
    }
}

package lt.kudze.stream.device.connector.messages.debezium;

import lt.kudze.stream.device.connector.messages.device.DeviceSensorKey;
import lt.kudze.stream.device.connector.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DebeziumDeviceSensorKey extends DebeziumKey<DeviceSensorKey> {
    public static final Serde<DebeziumDeviceSensorKey> serde = JsonSerdes.of(DebeziumDeviceSensorKey.class);
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package lt.kudze.stream.device.connector;

import lt.kudze.stream.device.connector.messages.debezium.DebeziumValue;
import lt.kudze.stream.device.connector.messages.device.DeviceKey;
import lt.kudze.stream.device.connector.messages.device.DeviceSensorValue;
import lt.kudze.stream.device.connector.messages.internal.DeviceConnectorAction;
import lt.kudze.stream.device.connector.messages.internal.InternalDeviceSensorValue;
import lt.kudze.stream.device.connector.messages.internal.InternalDeviceValue;
import lt.kudze.stream.device.connector.messages.internal.InternalGroupedDeviceSensorValue;
import lt.kudze.stream.device.connector.messages.output.OutputDeviceValue;
import lt.kudze.stream.device.connector.topics.DebeziumDeviceSensorTopic;
import lt.kudze.stream.device.connector.topics.DebeziumDeviceTopic;
import lt.kudze.stream.device.connector.topics.OutputDevicesTopic;
import lt.kudze.stream.device.connector.util.SystemUtil;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Named;
import org.apache.kafka.streams.state.KeyValueStore;

import java.time.Instant;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

/**
 * In this example, we implement a simple WordCount program using the high-level Streams DSL
 * that reads from a source topic "streams-plaintext-input", where the values of messages represent lines of text,
 * split each text line into words and then compute the word occurrence histogram, write the continuous updated histogram
 * into a topic "streams-wordcount-output" where each record is an updated count of a single word.
 */
public class DeviceConnectorStream {

    public static KTable<DeviceKey, InternalDeviceValue> buildInternalDeviceTable(StreamsBuilder builder) {
        var debeziumDeviceStream = builder.stream(DebeziumDeviceTopic.NAME, DebeziumDeviceTopic.CONSUMED);

        //It is given that deviceUuid cannot change.
        return debeziumDeviceStream.filter(
                (key, value) -> value != null,
                Named.as("devices-filter-tombstone")
        ).mapValues(
                DebeziumValue::getPayload,
                Named.as("devices-map-payload")
        ).filter(
                (key, value) -> {
                    //if either insert of delete we want this.
                    if (value.getBefore().isEmpty() || value.getAfter().isEmpty())
                        return true;

                    //On update we check if columns that interest us have changed:
                    var before = value.getBefore().get();
                    var after = value.getAfter().get();

                    return !before.getConfig().equals(after.getConfig()) ||
                            !before.getApiKeyHash().equals(after.getApiKeyHash());
                },
                Named.as("devices-filter-spam-updates")
        ).map(
                (key, value) -> new KeyValue<>(
                        key.getPayload(),
                        value.getAfter().map(device -> new InternalDeviceValue(
                                device,
                                InternalDeviceValue.computeNeededAction(
                                        device,
                                        value.getBefore().orElse(null)
                                )
                        )).orElse(null)
                ),
                Named.as("devices-map-to-internal")
        ).toTable(
                Named.as("internal-devices-to-table"),
                Materialized.<DeviceKey, InternalDeviceValue, KeyValueStore<Bytes, byte[]>>as("internal-devices-table")
                        .withKeySerde(DeviceKey.serde)
                        .withValueSerde(InternalDeviceValue.serde)
        );
    }

    public static KTable<DeviceKey, InternalGroupedDeviceSensorValue> buildInternalDeviceSensorTable(StreamsBuilder builder) {
        var debeziumDeviceSensorStream = builder.stream(DebeziumDeviceSensorTopic.NAME, DebeziumDeviceSensorTopic.CONSUMED);

        //It is given that deviceSensorUuid cannot change.
        //It is given that deviceUuid cannot change.
        return debeziumDeviceSensorStream.filter(
                (key, value) -> value != null,
                Named.as("sensors-filter-tombstone")
        ).mapValues(
                DebeziumValue::getPayload,
                Named.as("sensors-map-to-payload")
        ).filter(
                (key, value) -> {
                    //if either insert of delete we want this.
                    if (value.getBefore().isEmpty() || value.getAfter().isEmpty())
                        return true;

                    //On update we check if columns that interest us have changed:
                    var before = value.getBefore().get();
                    var after = value.getAfter().get();

                    return !before.getJsonpathQuery().equals(after.getJsonpathQuery());
                },
                Named.as("sensors-filter-spam-updates")
        ).map(
                (key, value) -> new KeyValue<>(
                        value.getAfter().map(DeviceSensorValue::constructDeviceKey).orElse(
                                value.getBefore().map(DeviceSensorValue::constructDeviceKey).orElse(null)
                        ),
                        value.getAfter().map(sensor -> new InternalDeviceSensorValue(
                                sensor,
                                InternalDeviceSensorValue.computeNeededAction(sensor, value.getBefore().orElse(null)),
                                sensor.getUpdatedAt(),
                                sensor.getUuid()
                        )).orElse(new InternalDeviceSensorValue(
                                null,
                                DeviceConnectorAction.DISCONNECT,
                                value.getBefore().map(DeviceSensorValue::getUpdatedAt).orElse(null),
                                value.getBefore().map(DeviceSensorValue::getUuid).orElse(null)
                        ))
                ),
                Named.as("sensors-map-to-internal-key-to-device")
        ).groupByKey(Grouped.with(
                "internal-sensors-grouped",
                DeviceKey.serde, InternalDeviceSensorValue.serde
        )).aggregate(
                InternalGroupedDeviceSensorValue::new,
                (aggKey, newValue, aggValue) -> {
                    //We have to aggregate the action too.
                    if (newValue.getLastModified() != null) {
                        if (aggValue.getLastModified() == null) {
                            aggValue.setLastModified(newValue.getLastModified());
                            aggValue.setNeededAction(newValue.getNeededAction());
                        } else {
                            var currentModifiedAt = Instant.parse(newValue.getLastModified());
                            var aggregationModifiedAt = Instant.parse(aggValue.getLastModified());

                            if (currentModifiedAt.isAfter(aggregationModifiedAt)) {
                                aggValue.setLastModified(newValue.getLastModified());
                                aggValue.setNeededAction(newValue.getNeededAction());
                            }
                        }
                    }

                    //Then just collect undeleted entries.
                    if (newValue.getSensorValue() == null) {
                        aggValue.getValues().remove(newValue.getSensorUuid());
                        return aggValue;
                    }

                    aggValue.getValues().put(newValue.getSensorValue().getUuid(), newValue.getSensorValue());
                    return aggValue;
                },
                Named.as("internal-sensors-keyed-to-devices-aggregated"),
                Materialized.<DeviceKey, InternalGroupedDeviceSensorValue, KeyValueStore<Bytes, byte[]>>as("internal-sensors-keyed-to-devices-aggregated")
                        .withKeySerde(DeviceKey.serde)
                        .withValueSerde(InternalGroupedDeviceSensorValue.serde)
        );
    }

    public static KTable<DeviceKey, OutputDeviceValue> buildOutputTable(StreamsBuilder builder) {
        var internalDeviceTable = buildInternalDeviceTable(builder);
        var internalDeviceSensorTable = buildInternalDeviceSensorTable(builder);

        return internalDeviceTable.leftJoin(
                internalDeviceSensorTable,
                (device, sensors) -> new OutputDeviceValue(
                        device,
                        OutputDeviceValue.computeSensorsFromGrouped(sensors),
                        OutputDeviceValue.computeNeededAction(device, sensors)
                ),
                Named.as("device-with-sensors-table"),
                Materialized.<DeviceKey, OutputDeviceValue, KeyValueStore<Bytes, byte[]>>
                                as("device-with-sensors-table")
                        .withKeySerde(DeviceKey.serde)
                        .withValueSerde(OutputDeviceValue.serde)
        );
    }

    public static StreamsBuilder buildTopology() {
        StreamsBuilder builder = new StreamsBuilder();

        buildOutputTable(builder).toStream(
                Named.as("output-to-stream")
        ).to(
                OutputDevicesTopic.NAME,
                OutputDevicesTopic.PRODUCED
        );

        return builder;
    }

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, SystemUtil.getenv("KAFKA_GROUP_ID"));
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, SystemUtil.getenv("KAFKA_BROKERS"));
        props.put(StreamsConfig.SECURITY_PROTOCOL_CONFIG, SystemUtil.getenv("KAFKA_SECURITY_PROTOCOL", "PLAINTEXT"));
        props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, SystemUtil.getenv("KAFKA_TRUSTSTORE_LOCATION", null));
        props.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, SystemUtil.getenv("KAFKA_TRUSTSTORE_PASSWORD", null));
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 100);
        props.put(StreamsConfig.producerPrefix(ProducerConfig.LINGER_MS_CONFIG), 0);

        final StreamsBuilder builder = DeviceConnectorStream.buildTopology();

        final Topology topology = builder.build();
        System.out.println(topology.describe());

        final KafkaStreams streams = new KafkaStreams(topology, props);
        final CountDownLatch latch = new CountDownLatch(1);

        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        try {
            streams.start();
            latch.await();
        } catch (Throwable e) {
            System.exit(1);
        }
        System.exit(0);
    }
}

package lt.kudze.stream.device.connector.messages.device;

import lt.kudze.stream.device.connector.serdes.StringSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DeviceSensorKey {
    public static final Serde<DeviceSensorKey> serde = StringSerdes.from(
            DeviceSensorKey::toString,
            DeviceSensorKey::new
    );

    private String uuid;

    public DeviceSensorKey() {
        this(null);
    }

    public DeviceSensorKey(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return this.uuid;
    }
}

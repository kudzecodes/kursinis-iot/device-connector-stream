package lt.kudze.stream.device.connector.messages.internal;

import lt.kudze.stream.device.connector.messages.device.DeviceSensorValue;
import lt.kudze.stream.device.connector.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class InternalDeviceSensorValue {
    public static final Serde<InternalDeviceSensorValue> serde = JsonSerdes.of(InternalDeviceSensorValue.class);

    private DeviceSensorValue sensorValue;
    private DeviceConnectorAction neededAction;
    private String lastModified;
    private String sensorUuid;

    public static DeviceConnectorAction computeNeededAction(DeviceSensorValue sensor, DeviceSensorValue beforeSensor) {
        //If before sensor is null, we want to reconnect the device. (new sensor added)
        if (beforeSensor == null)
            return DeviceConnectorAction.DISCONNECT;

        //Then if query has changed we want to reconnect the device.
        if (!sensor.getJsonpathQuery().equals(beforeSensor.getJsonpathQuery()))
            return DeviceConnectorAction.DISCONNECT;

        return DeviceConnectorAction.SYNC;
    }

    public InternalDeviceSensorValue() {

    }

    public InternalDeviceSensorValue(DeviceSensorValue value, DeviceConnectorAction neededAction, String lastModified, String sensorUuid) {
        this.sensorValue = value;
        this.neededAction = neededAction;
        this.lastModified = lastModified;
        this.sensorUuid = sensorUuid;
    }

    public DeviceConnectorAction getNeededAction() {
        return neededAction;
    }

    public void setNeededAction(DeviceConnectorAction neededAction) {
        this.neededAction = neededAction;
    }

    public DeviceSensorValue getSensorValue() {
        return sensorValue;
    }

    public void setSensorValue(DeviceSensorValue sensorValue) {
        this.sensorValue = sensorValue;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public String getSensorUuid() {
        return sensorUuid;
    }

    public void setSensorUuid(String sensorUuid) {
        this.sensorUuid = sensorUuid;
    }
}

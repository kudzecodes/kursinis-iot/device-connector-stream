package lt.kudze.stream.device.connector.messages.debezium;

import lt.kudze.stream.device.connector.messages.device.DeviceSensorValue;
import lt.kudze.stream.device.connector.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DebeziumDeviceSensorValue extends DebeziumValue<DeviceSensorValue> {
    public static final Serde<DebeziumDeviceSensorValue> serde = JsonSerdes.of(DebeziumDeviceSensorValue.class);
}

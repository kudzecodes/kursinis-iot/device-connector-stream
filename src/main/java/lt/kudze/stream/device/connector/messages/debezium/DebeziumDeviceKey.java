package lt.kudze.stream.device.connector.messages.debezium;

import lt.kudze.stream.device.connector.messages.device.DeviceKey;
import lt.kudze.stream.device.connector.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DebeziumDeviceKey extends DebeziumKey<DeviceKey> {
    public static final Serde<DebeziumDeviceKey> serde = JsonSerdes.of(DebeziumDeviceKey.class);
}
